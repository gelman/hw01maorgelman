#include <iostream>
#include "queue.h"

/*global variables*/
int arrSize = 0;
int numOfElements = 0;

/*
The function initializes the array in the queue
input: pointer to the queue and the size of the array
output: none
*/
void initQueue(queue* q, unsigned int size)
{
	if (size > 0)
	{
		arrSize = size;
		q->arr = new unsigned int[size]; /*initializing the array*/
	}
}

/*
The function free all the memory
input: pointer to the queue 
output: none
*/
void cleanQueue(queue* q)
{
	delete[] q->arr;
	delete(q);
	numOfElements = 0;
}

/*
The function adds new value to the end of the queue
input: pointer to the queue and new value
output: none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (arrSize > 0)
	{
		q->arr[numOfElements] = newValue; /*putting the new value in the array*/

		numOfElements++;
	}
}

/*
The function deletes value from the start of the queue
input: pointer to the queue
output: -1 empty queue and positive number in other situation
*/
int dequeue(queue* q)
{
	/*variables declaration*/
	int i = 0;
	unsigned int* temp = NULL;

	if (numOfElements > 0)
	{
		unsigned int* newArr = new unsigned int[arrSize]; /*initializing new array*/

		for (i = 0; i < numOfElements - 1; i++)
		{
			newArr[i] = q->arr[i+1]; /*putting the values in the right places*/
		}

		temp = q->arr;
		q->arr = newArr; /*saving the new pointer to the array*/

		delete[] temp; /*deleting the old array*/

		numOfElements--;

		return q->arr[0]; /*the value in the top of the queue*/
	}
	else
	{
		return -1; /*empty queue*/
	}
}