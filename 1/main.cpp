#include "queue.h"
#include <iostream>

int main()
{

	queue* q = new queue;

	initQueue(q, 20);


	for (int i = 1; i <= 20; i++)
	{
		enqueue(q, i);
	}

	std::cout << "printing all the values" << std::endl;

	for (int i = 0; i < 20; i++)
	{
		std::cout << i+1 << ": " << q->arr[i] << std::endl;
	}

	for (int i = 1; i <= 15; i++)
	{
		dequeue(q);
	}

	std::cout << "printing 5 last values" << std::endl;

	for (int i = 0; i < 5; i++)
	{
		std::cout << i + 1 << ": " << q->arr[i] << std::endl;
	}

	for (int i = 6; i <= 20; i++)
	{
		enqueue(q, i);
	}

	std::cout << "printing all the values" << std::endl;

	for (int i = 0; i < 20; i++)
	{
		std::cout << i + 1 << ": " << q->arr[i] << std::endl;
	}

	cleanQueue(q);

	getchar();
	return 0;
}