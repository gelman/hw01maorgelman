#include "linkedList.h"
#include <iostream>

int numOfNodes = 0;

firstStack* addNewNode(int num)
{
	firstStack* newNode = new firstStack; /*Dynamic allocation*/

	/*putting the values in the struct*/
	newNode->value = num;
	newNode->next = NULL;

	return newNode;
}


void insertNewNode(firstStack** head, int num)
{
	numOfNodes++;

	/*variable definition*/
	firstStack* curr = *head;
	firstStack* newNode = addNewNode(num);
	firstStack* temp = NULL;

	if (numOfNodes == 1)
	{
		*head = newNode;
	}
	else
	{
		/*connecting the nodes in the new order*/
		temp = *head;
		*head = newNode;
		(*head)->next = temp;
	}
}

void deleteNode(firstStack** head)
{
	/*variables definition*/
	firstStack* curr = *head;
	firstStack* temp = NULL;

	/*if the list is not empty (if list is empty - nothing to delete!)*/
	if (*head)
	{
		numOfNodes--;
		*head = (*head)->next;
		delete(curr);
	}
}

void freeList(firstStack** head)
{
	/*variables definition*/
	firstStack* temp = NULL;
	firstStack* curr = *head;

	while (curr)
	{
		temp = curr;
		curr = (curr)->next;
		delete(temp); /*free struct*/
	}

	*head = NULL;
	numOfNodes = 0;
}