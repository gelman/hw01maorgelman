
#include "stack.h"
#include <iostream>
#include "utils.h"

int main()
{
	int i = 0; 

	stack* s = new stack;
	stackNode* curr = NULL;

	initStack(s);


	for (i = 1; i <= 9; i++)
	{
		push(s, i);
	}

	curr = s->node;

	for (i = 1; i <= 9; i++)
	{
		std::cout << i << ": " << curr->value << std::endl;
		curr = curr->next;
	}

	pop(s);
	pop(s);

	std::cout << std::endl;

	curr = s->node;

	for (i = 1; i <= 7; i++)
	{
		std::cout << i << ": " << curr->value << std::endl;
		curr = curr->next;
	}

	cleanStack(s);


	getchar();
	return 0;
}


