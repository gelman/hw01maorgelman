#include "utils.h"
#include <iostream>
#include "stack.h"

#define ARR_SIZE 10

/*
The function reverses the array using stack
input: pointer to the array and the size of the array
output: none
*/
void reverse(int* nums, unsigned int size)
{
	/*variables declaration*/
	int i = 0;
	stack* s = new stack;
	s->node = new stackNode;

	/*pushing the elements to the stack*/
	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}

	/*putting the elements in the reverse order and poping the top element*/
	for (i = 0; i < size; i++)
	{
		nums[i] = s->node->value;
		pop(s);
	}

	cleanStack(s); /*frre all the memory*/
}


/*
The function gets from the user 10 integers and returns array that includes this numbers in the backwards order
input: none
output: pointer to array of integers
*/
int* reverse10()
{
	/*variables declarstion*/
	int* numbers = new int[ARR_SIZE]; 
	int i = 0;

	for (i = 0; i < ARR_SIZE; i++)
	{
		std::cout << "Please enter an integer number: ";
		std::cin >> numbers[i]; /*getting integers from the user*/
		std::cout << std::endl;
	}

	reverse(numbers, ARR_SIZE); /*reversing the array*/

	return numbers;
}