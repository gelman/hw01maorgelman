#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct firstStack
{
	unsigned int value;
	struct firstStack* next;

} firstStack;

firstStack* addNewNode(int num);
void insertNewNode(firstStack** head, int num);
void deleteNode(firstStack** head);
void freeList(firstStack** head);

#endif /* LINKED_LIST_H */