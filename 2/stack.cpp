#include "stack.h"
#include <iostream>

/*global variables*/
int countNodes = 0;

/*
The function initializes the first node in the stack
input: pointer to the stack
output: none
*/
void initStack(stack* s)
{
	s->node = new stackNode; /*dynamic allocation*/
	s->node->value = 0;
	s->node->next = NULL;
}

/*
The function free all the memory
input: pointer to the stack
output: none
*/
void cleanStack(stack* s)
{
	/*variables definition*/
	stackNode* temp = NULL;
	stackNode* curr = s->node;

	while (curr)
	{
		temp = curr;
		curr = curr->next;
		delete(temp); /*free the struct*/
	}
	delete(s);
	countNodes = 0;
}


/*
The function makes a new node
input: number
output: pointer to the new node
*/
stackNode* addNewNode(int num)
{
	stackNode* newNode = new stackNode; /*dynamic allocation*/

	/*putting the values in the struct*/
	newNode->value = num;
	newNode->next = NULL;

	return newNode;
}


/*
The function push new node to the list
input: pointer to the stack and number
output: none
*/
void push(stack* s, unsigned int element)
{
	countNodes++;

	/*variables declaration*/
	stackNode* newNode = addNewNode(element);
	stackNode* temp = NULL;

	if (countNodes == 1)
	{
		s->node = newNode;
	}
	else
	{
		/*connecting the nodes in the right order*/
		temp = s->node;
		s->node = newNode;
		newNode->next = temp;
	}

}


/*
The function pop npde from the list
input: pointer to the stack 
output: -1 for empty stack and 1 for successfully delete
*/
int pop(stack* s)
{
	/*variables definition*/
	stackNode* curr = s->node;
	stackNode* temp = NULL;

	/*if the list is not empty (if list is empty - nothing to delete!)*/
	if (curr)
	{
		countNodes--;
		s->node = s->node->next;
		delete(curr);

		return 1; /*deleted successfully*/
	}
	else
	{
		return -1; /*empty stack*/
	}

}